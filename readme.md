### Tetris
Tetris like game in the terminal. Needs work but it's playable.

![Tetris Screenshot](./docs/screenshot.png?raw=true "Screenshot")

### Get
```
$ git clone --recursive https://gitlab.com/_mobius/terminal-tetris.git
```

### Installing depenendencies
```
$ apt install libncurses5-dev libncursesw5-dev
```

### Compiling
```bash
$ mkdir build  
$ cd build  
$ cmake -DCMAKE_BUILD_TYPE=Release ..  
$ cmake --build .  
```

### Controls
* _pause_: __p__
* _move left_: __left arrow__
* _move right_: __right arrow__
* _rotate right_: __up arrow__
* _hold_: __tab__
* _quit_: __q__
* _reset/restart_: __x__
