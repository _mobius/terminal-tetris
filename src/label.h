/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef LABEL_H
#define LABEL_H

#include <string>
#include "tglib.h"

class Label
{
    public:


        Label( const std::string &text, const tgl::Vec2 &position, const tgl::Color_Pair &color = { tgl::Color::white, tgl::Color::none } ): color{ color }, _text{ text }, _position{ position } {}
        ~Label() = default;

        void show()
        {
            print( _position, color, _text );
        }

        void show( const tgl::Vec2 &pos )
        {
            _position = pos;
            show();
        }

        void hide()
        {
            print( _position, tgl::Color::none, std::string( _text.length(), ' ' ) );
        }

        tgl::Color_Pair color;

    private:
        std::string _text;
        tgl::Vec2   _position;
};


#endif