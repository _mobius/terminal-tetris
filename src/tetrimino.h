/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef TETRIMINO_H
#define TETRIMINO_H

#include <array>

#include "tglib.h"


template<size_t W, size_t H>
inline void rotate_90( const tgl::Color_Pair (&in)[H][W], tgl::Color_Pair (&out)[W][H] )
{
    for( uint32_t row = 0; row < W; ++row )
    {
        for( uint32_t col = 0; col < H; ++col )
        {
            out[row][col] = in[H - col - 1][row];
        }
    }

}

class Tetrimino
{
    public:
        struct Copy{};

        Tetrimino( std::array<tgl::Bitmap, 4> spr ) : bitmaps{ std::move( spr ) } {}

        template<size_t W, size_t H>
        Tetrimino( const tgl::Color_Pair (&c)[H][W] ) : 
            bitmaps{{ c, { W, H }, { H, W }, { W, H } }}
            {
                tgl::Color_Pair _r1[W][H]{};
                tgl::Color_Pair _r2[H][W]{};

                rotate_90( c, _r1 );
                bitmaps[1] = _r1;

                rotate_90( _r1, _r2 );
                bitmaps[2] = _r2;

                rotate_90( _r2, _r1 );
                bitmaps[3] = _r1;

                generation_segment_points();
            }

        template<size_t W, size_t H>
        Tetrimino( Copy, const tgl::Color_Pair (&c)[H][W] ) : bitmaps{{ c, c, c, c }} { generation_segment_points(); }

        void generation_segment_points()
        {
            auto seg_points = std::begin( segment_points );
            for( auto &bmp : bitmaps )
            {
                auto bmp_segpoint = std::begin( *seg_points );
                tgl::View view{ bmp };
                for( auto p = view.cbegin(), e = view.cend(); p != e; ++p )
                {
                    if( *p != bmp.transparent )
                    {
                        *bmp_segpoint = p.position();
                        ++bmp_segpoint;
                    }
                }

                ++seg_points;
            }
        }

        const tgl::Bitmap& default_view() const { return bitmaps[0]; }

        std::array<tgl::Bitmap, 4>   bitmaps;

        using segment_points_t = std::array<tgl::Vec2, 4>;
        std::array<segment_points_t, 4>  segment_points{};
    private:
        
};

inline std::vector<Tetrimino> tetriminos{};

#endif