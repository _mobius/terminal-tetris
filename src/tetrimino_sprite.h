/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef TETRIMINO_SPRITE_H
#define TETRIMINO_SPRITE_H

#include "tglib.h"
#include "tetrimino.h"

struct Rect_Vec2
{
    tgl::Vec2 ul;
    tgl::Vec2 lr;
};

inline Rect_Vec2 operator +( Rect_Vec2 rv, const tgl::Vec2 &v )
{
    rv.ul += v;
    rv.lr += v;
    return rv;
}

class Tetrimino_Sprite
{
    public:
        Tetrimino_Sprite( Tetrimino &tet = tetriminos[0], const tgl::Vec2 &spawn_point = { 2, -2 } ): tetrimino{ &tet }, position{ spawn_point } {};
        Tetrimino_Sprite( Tetrimino_Sprite && ) = default;
        Tetrimino_Sprite( const Tetrimino_Sprite & ) = default;
        ~Tetrimino_Sprite() = default;

        Tetrimino_Sprite& operator =( Tetrimino_Sprite && ) = default;
        Tetrimino_Sprite& operator =( const Tetrimino_Sprite & ) = default;

        void rotate_left()
        {
            if( !_index )
            {
                _index = 3;
            }
            else
            {
                --_index;
            }
        }

        void rotate_right()
        {
            if( _index >= 3 )
            {
                _index = 0;
            }
            else
            {
                ++_index;
            }
        }

        Rect_Vec2 bbox() const
        {
            const auto bp = position;
            return {
                bp,
                bp + tgl::Vec2{ bitmap().width(), bitmap().height() }
            };
        }

        Tetrimino::segment_points_t& segments()
        {
            return tetrimino->segment_points[ _index ];
        }

        const Tetrimino::segment_points_t& segments() const
        {
            return tetrimino->segment_points[ _index ];
        }

        tgl::Bitmap& bitmap()
        {
            return tetrimino->bitmaps[ _index ];
        }

        const tgl::Bitmap& bitmap() const
        {
            return tetrimino->bitmaps[ _index ];
        }

        Tetrimino   *tetrimino;
        tgl::Vec2    position;
    private:
        uint32_t _index{ 0 };
};


#endif