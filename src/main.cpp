/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <cassert>
#include <string>
#include <thread>
#include <set>
#include <algorithm>
#include <deque>

#include "tglib.h"

#include "label.h"
#include "tetrimino.h"
#include "tetrimino_sprite.h"

tgl::NCurses _nc{};

using collision_offsets_t = std::array<tgl::Vec2, 8>;

constexpr collision_offsets_t tetrimino_collision_offsets_l{{
    { -1, -1 }, { 0, -1 }, { 1, -1 },
    { -1,  0 },            { 1,  0 },
    { -1,  1 }, { 0,  1 }, { 1,  1 }
}};
constexpr collision_offsets_t tetrimino_collision_offsets_r{{
    { 1, -1 }, { 0, -1 }, { -1, -1 },
    { 1,  0 },            { -1,  0 },
    { 1,  1 }, { 0,  1 }, { -1,  1 }
}};

struct Game_State
{   
    Tetrimino_Sprite current_tetrimino{};

    std::deque<Tetrimino*> tet_bag{};

    Tetrimino *holding{ nullptr };
    bool       hold_used{ false };

    uint64_t score{};
    uint32_t level{ 1 };

    bool game_over{ false };
    bool paused{ false };

    tgl::time_point last_drop{ tgl::clock::now() };
    tgl::time_point lock_in_start{ tgl::clock::now() + std::chrono::hours{ 1 } }; // set when a downward collision happens

    int32_t downward_collisions{};
    int32_t puzzle_top{ std::numeric_limits<int32_t>::max() };

    std::chrono::milliseconds fall_wait{ 500 };
};

class Game
{
    public:
        Game()
        {
            using namespace tgl;

            fill( blt::transparent, playfield_border, playfield, { 1, 0 }, Color::none );
            blit( interface, playfield_border, { 8, 0 } );

            // Show UI Labels
            controls.show();
            score.show();
            hold.show();
            next.show();

            fill_bag();
            take_piece();
        }

        void reinit()
        {
            state = Game_State{};

            fill( playfield, tgl::Color::none );
            fill( hold_area, tgl::Color::none );

            fill_bag();
            take_piece();

            game_over.hide();

            set_pause( true );
        }

        void present()
        {
            interface.present();
            playfield.present();
        }

        void set_pause( bool pval )
        {
            state.paused = pval;

            if( state.paused )
            {
                pause.show();
            }
            else
            {
                pause.hide();
            }
        }

        void toggle_pause()
        {
            set_pause( !state.paused );
        }

        void shift_puzzle_down( int32_t top_y, int32_t bottom_y, int32_t shift = 1 )
        {
            using namespace tgl;
            // this is for collapsing a row once it's been completed.
            // - from completed row to the top of the puzzle stack we copy the image to a buffer, then back shifted

                // copy to buffer
            View copy_view{ playfield, 0, top_y, playfield.width(), bottom_y };
            Bitmap buffer{ copy_view.width(), copy_view.height(), Color::none };
            blit( buffer, copy_view, { 0, 0 } ); 

                // clear top of shift area
            fill( View{ playfield, 0, top_y, playfield.width(), top_y + shift }, Color::none ); // clear any left behind color

                // copy back shifted
            blit( blt::full, View{ playfield, 0, top_y + shift, playfield.width(), bottom_y + shift }, buffer, { 0, 0 } );
        }

        std::pair<tgl::Vec2, bool> will_collide_puzzle( const Tetrimino_Sprite &tsprite, const tgl::Vec2 &move = {} )
        {
            auto bbox = tsprite.bbox() + move;
            for( auto segment : tsprite.segments() )
            {
                segment = segment + bbox.ul;
                if( auto [px, valid] = playfield.pixel_at( segment.x, segment.y ); valid && *px != playfield.transparent )
                {
                    return { segment, true };
                }
            }

            return { {}, false };
        }

        int32_t will_collide_wall( const Tetrimino_Sprite &tsprite, const tgl::Vec2 &move = {} )
        {
            auto bbox = tsprite.bbox() + move;
            int32_t max_x{ std::numeric_limits<int32_t>::min() };
            int32_t min_x{ std::numeric_limits<int32_t>::max() };

            for( const auto &segment : tsprite.segments() )
            {
                max_x = std::max( max_x, (bbox.ul.x + segment.x) - playfield.width() ); // right wall, find segment nearest to
                min_x = std::min( min_x, bbox.ul.x + segment.x ); // left wall
            }

            if( max_x >= 0 ) // right wall collision
            {
                return -(max_x + 1); // amount to add to x to fix collision with wall
            }
            if( min_x < 0 ) // left wall collision
            {
                return -min_x;
            }

            return 0;
        }

        int32_t will_collide_floor_ceil( const Tetrimino_Sprite &tsprite, const tgl::Vec2 &move = {} )
        {
            auto bbox = tsprite.bbox() + move;
            int32_t max_y{ std::numeric_limits<int32_t>::min() };
            int32_t min_y{ std::numeric_limits<int32_t>::max() };
            for( const auto &segment : tsprite.segments() )
            {
                // looking for the tetrimino segment (color inner block) that is closest to the bottom
                max_y = std::max( max_y, (bbox.ul.y + segment.y) - playfield.height() );
                min_y = std::min( min_y, bbox.ul.y + segment.y ); // ceiling
            }

            // bottom segment is at or below collision line
            if( max_y >= 0 ) // 
            {
                return -(max_y + 1); // amount need to add to y to fix collision
            }
            if( min_y < -2 ) // left wall collision
            {
                return -min_y;
            }

            return 0;
        }

        void fill_bag()
        {
            while( state.tet_bag.size() <= 7 )
            {
                std::array<uint32_t, 7> bag{};
                std::iota( bag.begin(), bag.end(), 0 );
                std::shuffle( bag.begin(), bag.end(), piece_rand.generator() );

                for( auto index : bag )
                {
                    state.tet_bag.push_back( &tetriminos[ index ] );
                }
            }
        }

        void take_piece()
        {
            using namespace tgl;
            fill( preview_area, Color::none );

            state.current_tetrimino = Tetrimino_Sprite{ *state.tet_bag.front() };
            state.tet_bag.pop_front();
            fill_bag();

            int32_t y{ -1 };
            for( auto i = state.tet_bag.begin(), e = i + 3; i != e; ++i )
            {
                blit( preview_area, (*i)->default_view(), { 0, y } );
                y += 4;
            }
        }

        bool spawn_tetrimino( Tetrimino *tet = nullptr )
        {
            if( !tet )
            {
                take_piece();
            }
            else
            {
                state.current_tetrimino = Tetrimino_Sprite{ *tet };
            }
            
            if( will_collide_puzzle( state.current_tetrimino ).second )
            {
                state.game_over = true;
                for( auto &offset : tetrimino_collision_offsets_r )
                {
                    if( !will_collide_puzzle( state.current_tetrimino, offset ).second )
                    {
                        state.current_tetrimino.position += offset;
                        state.game_over = false;
                        return false;   // couldn't spawn
                    }
                }
            }

            return true;
        }

        void hold_piece()
        {
            using namespace tgl;
            if( state.hold_used ) { return; }

            auto temp = state.current_tetrimino.tetrimino;
            spawn_tetrimino( state.holding );
            state.holding = temp;
            state.hold_used = true;
            
            fill( hold_area, Color::none );
            blit( hold_area, state.holding->default_view(), { 0, 0 } );
        }

        std::pair<tgl::Vec2, bool> rotate_check( const Tetrimino_Sprite &tsprite, const collision_offsets_t &offsets )
        {
            tgl::Vec2 boundry_kick{ will_collide_wall( tsprite ), will_collide_floor_ceil( tsprite ) };

            auto [point, puzzle_collide] = will_collide_puzzle( tsprite, boundry_kick );

            // can we fix the collisions without colliding with the puzzle?
            if( !puzzle_collide )
            {
                return { boundry_kick, true };
            }
            else
            {
                for( const auto &offset : offsets )
                {
                    const auto kick = boundry_kick + offset;
                    if( !will_collide_wall( tsprite, kick ) && will_collide_floor_ceil( tsprite, kick ) <= 0 && !will_collide_puzzle( tsprite, kick ).second )
                    {
                        return { kick, true };
                    }
                }
            }

            return { {}, false };
        }

        void rotate_piece( void (Tetrimino_Sprite::*rotate)(), const collision_offsets_t &offsets )
        {
            auto tcopy{ state.current_tetrimino };
            (tcopy.*rotate)();

            if( auto [kick, can_rotate] = rotate_check( tcopy, offsets ); can_rotate )
            {
                (state.current_tetrimino.*rotate)();
                state.current_tetrimino.position += kick;
                state.lock_in_start = tgl::clock::now();
            }
        }

        void move_piece( tgl::Vec2 move )
        {
            if( move.x )
            {
                if( will_collide_wall( state.current_tetrimino, { move.x, 0 } ) || will_collide_puzzle( state.current_tetrimino, { move.x, 0 } ).second )
                {
                    move.x = 0;
                }
            }

            if( move.y )
            {
                state.last_drop = tgl::clock::now();

                if( will_collide_floor_ceil( state.current_tetrimino, move ) < 0 || will_collide_puzzle( state.current_tetrimino, move ).second )
                {
                    move.y = 0;
                    if( !state.downward_collisions )  // start the lock in countdown on first collision
                    {
                        state.lock_in_start = tgl::clock::now();
                    }
                    ++state.downward_collisions;
                }
                else
                {
                    state.downward_collisions = 0;
                }
            }

            state.current_tetrimino.position += move;
        }

        int32_t clear_completed_rows( )
        {
            using namespace tgl;
            // * get rows
            std::set<int32_t> y_check{};
            const auto bbox = state.current_tetrimino.bbox();
            for( const auto &segment : state.current_tetrimino.segments() )
            {
                const auto y = segment.y + bbox.ul.y;
                state.puzzle_top = std::min( state.puzzle_top, y );
                y_check.emplace( y );
            }

            // * check rows for completion
            for( const auto &y : y_check )
            {
                View row_view{ playfield, Rect{ 0, y, playfield.width(), 1 } };
                for( auto &p : row_view )
                {
                    if( p == playfield.transparent )
                    {
                        y_check.erase( y_check.find( y ) );
                        break;
                    }
                }
            }
    
            // * erase any complete rows
            uint32_t score_add{ 100 };
            uint32_t score_accum{0};
            for( const auto &y : y_check )
            {
                shift_puzzle_down( state.puzzle_top, y, 1 );
                
                score_accum += score_add;
                score_add += 200;
            }
            if( y_check.size() == 4 ) { score_accum += 100; }
            state.score += score_accum * state.level;
            print( Vec2{ 0, 1 }, "%15d", state.score );

            state.puzzle_top += y_check.size();

            return y_check.size();
        }

        void lock_in_check()
        {
            using namespace tgl;
            if( state.downward_collisions > 1 && (tgl::clock::now() - state.lock_in_start > std::chrono::milliseconds{ 500 }) )
            {
                state.downward_collisions = 0;
                state.hold_used = false;

                // redraw piece so it's locked in place
                blit( playfield, state.current_tetrimino.bitmap(), state.current_tetrimino.position );

                clear_completed_rows();

                if( state.puzzle_top <= 0 )
                {
                    state.game_over = true;
                    game_over.show();
                }
                else 
                {
                    spawn_tetrimino();
                }
            }
        }

    /*
            ==========
    */

        tgl::Surface interface{ 28, 25, 0, 0 };
        tgl::Surface playfield{ 10, 20, 18, 0, tgl::Color::none };      // draw moving/locked pieces here
        tgl::Bitmap  playfield_border{ 12, 21, tgl::Color::yellow };

        tgl::View<tgl::Surface> hold_area{ interface, { 2, 3, 5, 5 } };
        tgl::View<tgl::Surface> preview_area{ interface, { 2, hold_area.lower_right().y + 1, 5, 15 } };

        tgl::Color_Pair label_color{ tgl::Color::green, tgl::Color::none };

        Label hold{ "Hold", { 7, 2 }, label_color };
        Label next{ "Next", { 7, hold_area.lower_right().y }, label_color };
        Label score{ "Score", { 6, 0 }, label_color };
        Label pause{ "Paused", { 25, 5 }, label_color };
        Label controls{ "Controls", { 42, 0 }, label_color };
        Label game_over{ "Game Over", { 23, 5 }, { tgl::Color::red, tgl::Color::none } };

        Game_State state{};
        tgl::Random<uint8_t> piece_rand{ 0, 6 };
};

int main()
{
    using namespace tgl;

    // Create tetriminos
    []{
        const tgl::Color_Pair t{ },
                    c{ tgl::Color::cyan },
                    b{ tgl::Color::blue },
                    w{ tgl::Color::white },
                    y{ tgl::Color::yellow },
                    g{ tgl::Color::green },
                    m{ tgl::Color::magenta },
                    r{ tgl::Color::red };

        std::array<Tetrimino, 7> tets{{
            // I
            {{
                {t,t,t,t,t},
                {t,t,t,t,t},
                {c,c,c,c,t},
                {t,t,t,t,t},
                {t,t,t,t,t}
            }},
            // J
            {{
                {t,t,t,t,t},
                {t,t,t,t,t},
                {t,b,b,b,t},
                {t,t,t,b,t},
                {t,t,t,t,t}
            }},
            // L
            {{
                {t,t,t,t,t},
                {t,t,t,t,t},
                {t,w,w,w,t},
                {t,w,t,t,t},
                {t,t,t,t,t}
            }},
            // O
            { Tetrimino::Copy{}, {
                {t,t,t,t,t},
                {t,t,t,t,t},
                {t,t,y,y,t},
                {t,t,y,y,t},
                {t,t,t,t,t}
            }},
            // S
            {{
                {t,t,t,t,t},
                {t,t,t,t,t},
                {t,t,g,g,t},
                {t,g,g,t,t},
                {t,t,t,t,t}
            }},
            // T
            {{
                {t,t,t,t,t},
                {t,t,m,t,t},
                {t,m,m,m,t},
                {t,t,t,t,t},
                {t,t,t,t,t}
            }},
            // Z
            {{
                {t,t,t,t,t},
                {t,t,t,t,t},
                {t,r,r,t,t},
                {t,t,r,r,t},
                {t,t,t,t,t}
            }}
        }};

        tetriminos.insert( tetriminos.end(), tets.begin(), tets.end() );
    }();

    Game      game{};
    Input_Map input{};

    // Setup input map
    [&input, &game]
    {
        input.map_key( 'j', "move_right" );
        input.map_key( KEY_RIGHT, "move_right" );
        input.map_key( 'g', "move_left" );
        input.map_key( KEY_LEFT, "move_left" );
        input.map_key( 'h', "move_down" );
        input.map_key( KEY_DOWN, "move_down" );
        input.map_key( 'u', "rotate_right" );
        input.map_key( KEY_UP, "rotate_right" );
        input.map_key( 't', "rotate_left" );
        input.map_key( ' ', "drop" );
        input.map_key( '\t', "hold" );
        input.map_key( '\n', "hold" );
        input.map_key( 'p', "pause" );
        input.map_key( 'x', "restart" );
        input.map_key( 'q', "quit" );

        // print input
        int y = 0;
        constexpr const char *controls[]{ 
            "move right  : j, <right arrow>",
            "move left   : g, <left arrow>",
            "move down   : h, <down arrow>",
            "rotate right: u, <up arrow>",
            "rotate left : t",
            "hold        : <enter>, <tab>",
            "pause       : p",
            "restart     : x",
            "quit        : q"
        };
        for( auto &ctrl : controls )
        {
            print( Vec2{ 42, ++y }, ctrl );
        }
    }();

    while( !input.is_active( "quit" ) )
    {
        input.update();

        if( input.is_active( "pause" ) )
        {
            game.toggle_pause();
        }

        if( input.is_active( "restart" ) )
        {
            game.reinit();
        }

        if( !game.state.game_over && !game.state.paused )
        {
            // clear tetrimino from playfield
            fill( blt::opaque, game.playfield, game.state.current_tetrimino.bitmap(), game.state.current_tetrimino.position, Color::none);

            // hold currently active piece  
            if( input.is_active( "hold" ) )
            {
                game.hold_piece();
            }
            
            if( input.is_active( "rotate_right" ) )
            {
                game.rotate_piece( &Tetrimino_Sprite::rotate_right, tetrimino_collision_offsets_r );
            }
            else if( input.is_active( "rotate_left" ) )
            {
                game.rotate_piece( &Tetrimino_Sprite::rotate_left, tetrimino_collision_offsets_l );
            }

            const auto move_after_delay = (tgl::clock::now() - game.state.last_drop) > game.state.fall_wait;
            Vec2 move{
                input.is_active( "move_right" ) - input.is_active( "move_left" ),
                input.is_active( "move_down" ) || move_after_delay
            };

            game.move_piece( move );
            game.lock_in_check();

            blit( game.playfield, game.state.current_tetrimino.bitmap(), game.state.current_tetrimino.position );
        }

        game.present();
        std::this_thread::sleep_for( std::chrono::milliseconds( 35 ) );
    }
    
    return 0;
}
